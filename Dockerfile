FROM pytorch/pytorch:1.8.1-cuda11.1-cudnn8-runtime

RUN apt update
RUN apt install -y gcc zlib1g-dev make cmake build-essential

COPY ./scripts/requirements.txt .
COPY ./scripts/rllib_requirements.txt .

RUN pip install -r requirements.txt
RUN pip install -r rllib_requirements.txt
RUN pip install ray[rllib]

RUN apt install -y libgtk2.0-dev

RUN pip install 'ray[default]'
RUN pip install aioredis==1.3

RUN chmod -R 777 /opt/conda/lib/python3.8/site-packages/ray/rllib/policy/

ENV TERM=xterm
